# Lyra
A custom mobile application for the Polish online grading system, Librus.

# Installing build dependencies
First, install `pipx` using your preferred method.
Then, install `poetry` (used for dependency management) and `pre-commit` with pipx:
```bash
pipx install poetry pre-commit
# Alternatively, you can use pip with --break-system-packages (not recommended):
# pip install poetry pre-commit --break-system-packages
```
Afterwards, install the project's dependencies:
```bash
poetry install
```
Furthermore, enable the pre-commit formatting hooks:
```bash
pre-commit install
```
And you're done! You can additionally set up your editor to use `ruff` and `ruff-lsp` for linting/static analysis/formatting.
